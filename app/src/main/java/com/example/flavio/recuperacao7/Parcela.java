package com.example.flavio.recuperacao7;

public  class Parcela {
    private int ordemParcela;
    private double juros;
    private double amortizacao;
    private double saldoDevedor;

    public Parcela() {

    }

    public Parcela(int ordemParcela,double juros , double amortizacao, double saldoDevedor) {
        this.ordemParcela=ordemParcela;
        this.juros=juros;
        this.amortizacao=amortizacao;
        this.saldoDevedor=saldoDevedor;
    }

    public double getValorPrestacao(){
        return this.amortizacao+this.juros;
    }

    public int getOrdemParcela() {
        return ordemParcela;
    }

    public double getJuros() {
        return juros;
    }

    public double getAmortizacao() {
        return amortizacao;
    }

    public double getSaldoDevedor() {
        return saldoDevedor;
    }
}