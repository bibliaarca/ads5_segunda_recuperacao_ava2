package com.example.flavio.recuperacao7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcular____(View view) {

        Intent i = new Intent(this, ClasseCalcular.class);
        EditText edMeses = (EditText) findViewById(R.id.edMeses);
        EditText edValor = (EditText) findViewById(R.id.edValor);
        EditText edJuros = (EditText) findViewById(R.id.edJuros);
        RadioButton rbSac = (RadioButton) findViewById(R.id.rbSac);
        RadioButton rbPrice = (RadioButton) findViewById(R.id.rbPrice);
        Bundle params = new Bundle();
        params.putInt("edMeses", Integer.parseInt(edMeses.getText().toString()));
        params.putFloat("edValor", Float.parseFloat(edValor.getText().toString()));
        params.putFloat("edJuros", Float.parseFloat(edJuros.getText().toString()));
        params.putBoolean("rbSac", rbSac.isChecked());
        params.putBoolean("rbPrice", rbPrice.isChecked());
        i.putExtras(params);

        startActivity(i);

    }
}
