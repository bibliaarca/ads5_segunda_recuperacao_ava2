package com.example.flavio.recuperacao7;

import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public   class Financiamento {
    public List<Parcela> parcelas;
    double  valorFinanciamento;
    double  txJuros;
    int     nParcelas;
    double  prestacao;
    public Financiamento(int nParcelas, double valorFinanciamento, double txJuros) {
        this.txJuros=txJuros;
        this.valorFinanciamento=valorFinanciamento;
        this.nParcelas=nParcelas;
        this.parcelas=new ArrayList<Parcela>();

        calculaPrimeiraParcela(this.nParcelas,this.valorFinanciamento,this.txJuros);
        calculaPrestacao(this.nParcelas,this.valorFinanciamento,this.txJuros);
        calculaParcelas();

    }

    public void calculaPrimeiraParcela(int nParcelas, double valorFinanciamento, double txJuros){
        this.parcelas.add(new Parcela(0,0,0,valorFinanciamento));
    }

    public void calculaParcelas() {
        int i = 1;
        for (i = 1; i <=this.nParcelas; i++) {
            this.parcelas.add(new Parcela(i,
                    this.parcelas.get(i - 1).getSaldoDevedor() *this.txJuros,
                    this.prestacao-(this.parcelas.get(i - 1).getSaldoDevedor() *this.txJuros),
                    this.parcelas.get(i - 1).getSaldoDevedor() -(this.prestacao-(this.parcelas.get(i - 1).getSaldoDevedor() *this.txJuros))));
        }

    }
    public void calculaPrestacao (int nParcelas, double valorFinanciamento, double txJuros){
        double txJurosI2=1+this.txJuros;
        double etapa1=(Math.pow(txJurosI2,nParcelas))*txJuros;
        double etapa2=(Math.pow(txJurosI2,nParcelas))-1;
        double prestacao= this.valorFinanciamento *(etapa1/etapa2);
        //Log.i("Prestacao:", " "+(etapa1/etapa2)+"   "+prestacao);
        this.prestacao=prestacao;
    }


    public ArrayList<Parcela> getParecelas(){
        return (ArrayList<Parcela>) this.parcelas;
    }




}
