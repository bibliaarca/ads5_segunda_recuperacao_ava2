package com.example.flavio.recuperacao7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class ClasseCalcular extends AppCompatActivity {

/*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classe_calcular);
    }
*/
    private ArrayList<HashMap<String, String>> list;
    public static final String COLUNA_1 = "#";
    public static final String COLUNA_2 = "Parcelas";
    public static final String COLUNA_3 = "Amortizações";
    public static final String COLUNA_4 = "Juros";
    public static final String COLUNA_5 = "Saldo Devedor";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classe_calcular);

        ListView listView=(ListView)findViewById(R.id.listView1);
        populateList();
        ListViewAdapter adapter=new ListViewAdapter(this, list);
        listView.setAdapter(adapter);

    }

    private void populateList() {

        // Recuperando dados dos parâmetros da tela
        Intent i=getIntent();
        int edMeses = i.getExtras().getInt("edMeses");
        float edValor = i.getExtras().getFloat("edValor");
        float edJuros = i.getExtras().getFloat("edJuros");
        boolean rbSac = i.getExtras().getBoolean("rbSac");
        boolean rbPrice = i.getExtras().getBoolean("rbPrice");


        Intent intent=getIntent();
        int meses=intent.getExtras().getInt("edMeses");
        float valorEmprestimo=intent.getExtras().getFloat("edValor");
        float txJuros=intent.getExtras().getFloat("edJuros");

        Financiamento f=  new Financiamento( meses, valorEmprestimo, txJuros);

        // inicialização de variáveis
        double soma = 0;
        int conta = -1;
        list=new ArrayList<HashMap<String,String>>();

        // Exibição do resultado da operação
        for (Parcela p: f.getParecelas()){
            ++conta;
            //p.ge
            if (conta == 0) {
                HashMap<String,String> hashmap=new HashMap<String, String>();
                hashmap.put(COLUNA_1, "");
                hashmap.put(COLUNA_2, "Parcelas");
                hashmap.put(COLUNA_3, "Amortizações");
                hashmap.put(COLUNA_4, "Juros");
                hashmap.put(COLUNA_5, "Saldo Devedor");
                list.add(hashmap);
            } else {
                HashMap<String,String> hashmap=new HashMap<String, String>();
                hashmap.put(COLUNA_1, String.valueOf(conta));
                hashmap.put(COLUNA_2, NumberFormat.getCurrencyInstance().format(p.getValorPrestacao()));
                hashmap.put(COLUNA_3, NumberFormat.getCurrencyInstance().format(p.getAmortizacao()));
                hashmap.put(COLUNA_4, NumberFormat.getPercentInstance().format(p.getJuros()));

                hashmap.put(COLUNA_5, String.valueOf(p.getAmortizacao()));
                list.add(hashmap);
            }
            soma+=p.getValorPrestacao();
        }




    }


}


